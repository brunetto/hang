module gitlab.com/brunetto/hang

require (
	github.com/GeertJohan/go.rice v0.0.0-20170420135705-c02ca9a983da // indirect
	github.com/MarkSonghurst/swag v0.0.0-20170613060156-c2ad330665ad // indirect
	github.com/brunetto/gin-logrus v0.0.0-20170921123611-3ab5813db8fd
	github.com/daaku/go.zipexe v0.0.0-20150329023125-a5fe2436ffcb // indirect
	github.com/gin-contrib/sse v0.0.0-20170109093832-22d885f9ecc7 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/go-openapi/runtime v0.17.2 // indirect
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/json-iterator/go v1.1.5 // indirect
	github.com/kardianos/osext v0.0.0-20170510131534-ae77be60afb1 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.2.0
	github.com/ugorji/go/codec v0.0.0-20181022190402-e5e69e061d4f // indirect
	gitlab.com/brunetto/ritter v0.0.0-20170921135153-e5b5f3366504
	gitlab.com/brunetto/swaggo v0.0.0-20170828142316-394e10f2d12f
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
)
